<?php
namespace Kimlu\WP\Plugin;

use ErrorException;
use kimlu\utils\Path;

class WPPluginEnv
{

	/**
	 * 
	 * @var array
	 */
	static private array $pluginsEnv = [];
	
	/**
	 * 
	 * @var string
	 */
	static private ?string $currentPluginsEnv = NULL;
	
	/**
	 * 
	 */
	static public function init(): void
	{
		$backtrace = debug_backtrace( DEBUG_BACKTRACE_PROVIDE_OBJECT, 1 );
			
		$pluginEnv = new WPPluginEnv( $backtrace[ 0 ][ 'file' ] );
		
		self::$pluginsEnv[ $pluginEnv->urn() ] = $pluginEnv;
		self::current( $pluginEnv->urn() );		
	}

	/**
	 * 
	 * @param string $urn
	 * @throws ErrorException
	 * @return WPPluginEnv
	 */
	static public function current( string $urn = NULL ): WPPluginEnv
	{
		if ( isset( $urn ) && isset( self::$pluginsEnv[ $urn ] ) )
		{
			self::$currentPluginsEnv = $urn;
		}
		if ( isset( self::$currentPluginsEnv ) )
		{
			return self::$pluginsEnv[ self::$currentPluginsEnv ];
		}
		throw new ErrorException( "Error: The current Env is not selected." );
	}

	/**
	 * 
	 * @param string $urn
	 * @return WPPluginEnv
	 */
	static public function get( string $urn ): WPPluginEnv
	{
		return self::$pluginsEnv[ $urn ];
	}
	
	/**
	 * 
	 * @return string
	 */
	static public function dump(): string
	{
		$dump = static::class;
		$dump .= PHP_EOL;
		$dump .= print_r( self::$pluginsEnv, TRUE );
		$dump .= PHP_EOL;
		
		return $dump;
	}
	
	/**
	 * 
	 * @var string
	 */
	private string $urn = '';

	/**
	 * 
	 * @param string $urn
	 * @param string $wpPluginsPath
	 */
	private function __construct( string $pluginAbsoluteFilePath )
	{
		$this->urn = basename( dirname( $pluginAbsoluteFilePath ) );
	}
	
	/**
	 * 
	 * @return string
	 */
	public function urn(): string
	{
		return $this->urn;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function wpPluginsPath(): string
	{
		return Path::forceDirBase( WP_PLUGIN_DIR );
	}

	/**
	 * 
	 * @param string $relativePath
	 * @return string
	 */
	public function path( string $relativePath = NULL ): string
	{
		$path = Path::safe( "{$this->wpPluginsPath()}{$this->urn()}" );
		if ( isset( $relativePath ) )
		{
			$path = Path::forceDirBase( $path );
			$path .= Path::safe( $relativePath );
		} 
		return $path;
	}

	/**
	 * 
	 * @param string $extension
	 * @return string
	 */
	public function pathCSS( string $extension = NULL ): string
	{
		$path = "{$this->path()}/app/ui/css";
		if( isset( $extension ) )
		{
			$path = "{$path}/{$extension}";
		}
		return Path::safe( $path );
	}
	
	/**
	 * 
	 * @param string $extension
	 * @return string
	 */
	public function pathJS( string $extension = NULL ): string
	{
		$path = "{$this->path()}/app/ui/js";
		if( isset( $extension ) )
		{
			$path = "{$path}/{$extension}";
		}
		return Path::safe( $path );
	}
	
	/**
	 * 
	 * @param string $extension
	 * @return string
	 */
	public function url( string $extension = NULL ): string
	{
		$url = WP_PLUGIN_URL.'/'.$this->urn();
		if ( isset( $extension ) )
		{
			$url = "{$url}/{$extension}";
		}
		return $url;
	}
	
	/**
	 * 
	 * @param string $extension
	 * @return string
	 */
	public function urlJS( string $extension = NULL ): string
	{
		$url = "{$this->url()}/app/ui/js";
		if ( isset( $extension ) )
		{
			$url = "{$url}/{$extension}";
		}
		return $url;
	}

	/**
	 * 
	 * @param string $extension
	 * @return string
	 */
	public function urlCSS( string $extension = NULL ): string
	{
		$url = "{$this->url()}/app/ui/css";
		if ( isset( $extension ) )
		{
			$url = "{$url}/{$extension}";
		}
		return $url;
	}

}
